package sysboon.dev.imagezoomingdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.otaliastudios.zoom.ZoomImageView;
//this is exacttly for zooming option
public class ZoomingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zooming);
        final ZoomImageView zoomImage = findViewById(R.id.zoom_image);
        zoomImage.setImageDrawable(getResources().getDrawable(R.drawable.bucket1));


    }
}
